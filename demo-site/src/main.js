import './assets/main.css'
import { createApp } from 'vue'
import App from './App.vue'
import router from './router/index.js'
import store from './store'
import * as ElementPlusIconsVue from "@element-plus/icons-vue";
import "element-plus/theme-chalk/el-message.css";
import "element-plus/theme-chalk/el-message-box.css"; 
import 'normalize.css/normalize.css'
import i18n from './i18n/index';
import './mock'
import CmTable from "@/components/CmTable.vue"
const app = createApp(App);
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  app.component(key, component)
}
app.component(CmTable.name,CmTable);
app.use(router).use(store).use(i18n).mount('#app');
