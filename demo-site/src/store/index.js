import { createStore } from 'vuex'
import user from './modules/user'

export default createStore({
    state: {
        routeLoaded: false,
        firstRoute: null,
        menuTree: null,


    },
    mutations: {
        setRouteLoaded(state, loaded) {
            state.routeLoaded = loaded;
        },
        
        setFirstRoute(state, route) {
            state.firstRoute = route;
        },
        setMenuTree(state, data) {
            state.menuTree = data
        }
    },
    modules: {
        user
    }
})
