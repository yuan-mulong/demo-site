export const depts = [
    {
        deptId: 1,
        deptName: '大具集团'
    },
   
    {
        deptId: 2,
        deptName: 'wowo集团'
    },
];
export const appUsers = [
    {
        id: 1,
        deptId: 1,
        roleIds: "test",
        status:0,
        createTime: '@date @time'
    },
   
    {
        id: 2,
        deptId: 2,
        roleIds: "develop",
        status:1,
        createTime: '@date @time'
    },
];
export const roles = [
    {
        updatedBy:'admin',
        createdBy:'admin',
        name: "admin",
        label: '超级管理员'
    },
    {
        updatedBy:'admin',
        createdBy:'admin',
        name: "master",
        label: '应用管理员'
    },
    {
        updatedBy:'admin',
        createdBy:'admin',
        name: "visitor",
        label: '普通管理员'
    },
    {
        updatedBy:'admin',
        createdBy:'admin',
        name: "develop",
        label: '开发人员'
    },
    {
        updatedBy:'admin',
        createdBy:'admin',
        name: "test",
        label: '测试人员'
    },
    {
        updatedBy:'admin',
        createdBy:'admin',
        name: "project-manager",
        label: '项目经理'
    },
];
export const users = [
    {
        id: 1,
        name: "admin",
        roleId: "admin",
        password: "admin",
        createBy: "admin",
        createTime: '@date @time'
    },
    {
        id: 2,
        name: "master",
        roleId: "master",
        password: "master",
        createBy: "system",
        createTime: '@date @time'
    },
    {
        id: 3,
        name: "visitor",
        roleId: "visitor",
        password: "visitor",
        createBy: "system",
        createTime: '@date @time'

    },
    {
        id: 4,
        name: "test",
        roleId: "master",
        password: "test",
        createBy: "admin",
        createTime: '@date @time'

    }
]
export const menuTreeData = [
    {
        id: 1,
        parentId: 0,
        name: 'App',
        path: "/app",
        icon: "menu",
        children: [
            {
                id: 11,
                parentId: 1,
                name: 'AppUser',
                path: "/app/user",
                icon: "management",
            },
            {
                id: 12,
                parentId: 1,
                name: 'AppDept',
                path: "/app/dept",
                icon: "office-building",
            },
            {
                id: 13,
                parentId: 1,
                name: 'AppRole',
                path: "/app/role",
                icon: "avatar",
            },
            {
                id: 14,
                parentId: 1,
                name: 'AppResource',
                path: "/app/resource",
                icon: "management",
            },
         
        ]
    },
    {
        id: 2,
        parentId: 0,
        name: 'Sys',
        path: "/sys",
        icon: "setting",
        children: [
            {
                id: 21,
                parentId: 2,
                name: 'SysUser',
                path: "/sys/user",
                icon: "user-filled",
            },
            {
                id: 22,
                parentId: 2,
                name: 'SysNotice',
                path: "/sys/notice",
                icon: "chat-dot-round",
            },

        ]

    },
    {
        id: 3,
        parentId: 0,
        name: 'Logs',
        path: "/logs",
        icon: "document",
        children: [
            {
                id: 31,
                parentId: 3,
                name: 'LogsVisit',
                path: "/logs/visitr",
                icon: "tickets",
            },
            {
                id: 32,
                parentId: 3,
                name: 'LogsOperation',
                path: "/logs/operation",
                icon: "operation",
            },

        ],

    },


];


