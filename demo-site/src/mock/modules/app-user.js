import { roles, appUsers,depts } from '../data'
export function listPage() {
    return {
        url: "app/user/listPage",
        type: "get",
        response: (opst) => {
         
            const { pageNum, pageSize, name } = opst.data;
            let set = appUsers.map(v => {
                const o = { ...v };
                console.log('o',o.roleId)

                if (v.id <= 4) {
                    o.id="@increment",
                    o.name='@cname';
                    o.deptName =depts.find(item => item.deptId === o.deptId).deptName;
                    o.roleName = roles.find(item => item.name === o.roleIds).label;
                    o.email = '@email';
                    o.mobile = /1\d{10}/;
                }
                return o
            });
            console.log('dee',set)
          
            if (name) {
                set = set.filter(v => v.name === name)
            }
      
            const totalSize = set.length;
            const totalPages = Math.ceil(totalSize / pageSize);
            let lastIndex = pageNum * pageSize;
            if (lastIndex > totalSize) {
                lastIndex = totalSize;
            }
          console.log('set',set)
            let resData = [];
            if (pageNum >= 1 && pageSize >= totalPages) {
                resData = set.slice((pageNum - 1) * pageSize, lastIndex)
            }
            console.log('resData',resData)
            return {
                code: 200,
                msg: null,
                data: {
                    pageNum,
                    pageSize,
                    totalSize,
                    content: resData
                }
            }
        }
    }
}
export function save() {
    return {
      url: "app/user/save",
      method: "post",
      response: (opts) => {
        console.log('listPage()',listPage())
        return {
          code: 200,
          data: {
            name: opts.data.name,
            password: '@word(8,16)'
          }
        }
      }
    }
  }
export function operations() {

    return {
        url: "app/user/(update|delete)",
        type: "post",
        response: {
            code: 200,
        }

    }
}