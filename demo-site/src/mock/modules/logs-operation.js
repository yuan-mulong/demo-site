export function listPage() {
    return {
        url: "logs/operation/listPage",
        type: "get",
        response: (opst) => {
            const { pageNum, pageSize } = opst.data;
            const totalSize = 105;
            const key = pageNum * pageSize < totalSize ? `content|${pageSize}` : `content|${totalSize % pageSize}`
            return {
                code: 200,
                msg: null,
                data: {
                    pageNum,
                    pageSize,
                    totalSize,
                    [key]:[{
                        id:"@increment",
                        operTime:'@date @time',
                        "username|1":["admin","test01","test02",'@word'],
                        "status|1":["成功","失败"],
                        ip:"@ip",
                        duration:"@integer(0,1000)",
                        title:"sss"
                    }]
                }
            }
        }
    }
}