export function listPage() {
    return {
        url: "sys/notice/listPage",
        type: "get",
        response: (opst) => {
            const { pageNum, pageSize } = opst.data;
            const totalSize = 4;
            const content = pageNum * pageSize < totalSize ? `content|${pageSize}` : `content|${totalSize % pageSize}`
            return {
                code: 200,
                msg: null,
                data: {
                    pageNum,
                    pageSize,
                    totalSize,
                    [content]: [{
                        id: "@increment",
                        'createdBy|1': ["admin", "test01", "test02"],
                        title: "@ctitle(5,10)",
                        createdTime: '@date @time',
                        content: "@cparagraph(1,2)",
                        'publishTime|1': ["", '@date @time'],

                    }]
                }
            }
        }
    }
}
export function operations() {
    return {
        url: "sys/notice/(save|update|delete)",
        type: "post",
        response:{
            code:200,
        }
     
    }
}