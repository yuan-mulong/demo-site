export function listTree() {
    let findTreeData = {
        code: 200,
        msg: null,
        data: [
            {
                id: 37,
                name: 'monitor',
                displayName: '系统监控',
                icon: 'info',
                type: 0,
                url: '',
                orderNum: 4,
                parentId: null,
                parentNmae: null,
                level: 0,
                children: [
                    {
                        id: 38,
                        name: 'summary',
                        displayName: '统计',
                        icon: 'warning',
                        type: 1,
                        url: '/monitor/summary',
                        orderNum: 0,
                        parentId: 1,
                        level: 1,
                        children: []
                    },
                    {
                        id: 39,
                        name: 'data',
                        displayName: '数据监控',
                        icon: 'warning',
                        type: 1,
                        url: '/monitor/data',
                        orderNum: 0,
                        parentId: 1,
                        level: 1,
                        children: [
                            {
                                id: 40,
                                name: 'view',
                                displayName: '查看',
                                icon: '',
                                type: 2,
                                url: '',
                                orderNum: 0,
                                parentId: 39,
                                level: 2,
                                children: []
                            },
                        ]
                    }
                ]
            }
        ]
    }

    return {
        url: "app/resource/listTree",
        method: 'get',
        response: findTreeData,
    }
}


export function listTreeParents() {
    return ({
        url: "/app/resource/listTreeParents",
        method: 'get',
        response: () => {
            function filterTree(data) {
                const newtree = data.filter(v => v.type !== 2)
                newtree.forEach(v => v.children && (v.children = filterTree(v.children)))
                return newtree
            }
            return {
                code: 200,
                data: filterTree(JSON.parse(JSON.stringify(treeData)))
            }
        }
    })
}
export function operations() {
    return {
        url: "app/resource/(save|update|delete)",
        type: "post",
        response: {
            code: 200,
        }

    }
}