import { roles, users } from '../data'
export function listPage() {
    return {
        url: "sys/user/listPage",
        type: "get",
        response: (opst) => {
         
            const { pageNum, pageSize, name } = opst.data;
            let set = users.map(v => {
                const o = { ...v };
                if (v.id <= 4) {
                    o.password = o.name;
                    o.roleNmae = roles.find(item => item.name === o.roleId).label;
                    o.ip = '@ip';
                    o.visitTime = '@date @time'
                }
                return o
            });
          
            if (name) {
                set = set.filter(v => v.name === name)
            }
      
            const totalSize = set.length;
            const totalPages = Math.ceil(totalSize / pageSize);
            let lastIndex = pageNum * pageSize;
            if (lastIndex > totalSize) {
                lastIndex = totalSize;
            }
          console.log('set',set)
            let resData = [];
            if (pageNum >= 1 && pageSize >= totalPages) {
                resData = set.slice((pageNum - 1) * pageSize, lastIndex)
            }
            
            return {
                code: 200,
                msg: null,
                data: {
                    pageNum,
                    pageSize,
                    totalSize,
                    content: resData
                }
            }
        }
    }
}
export function save() {
    return {
        url: "sys/user/save",
        type: "post",
        response: (opst) => {
            return{
                code:200,
                data:{
                    name:opst.data.name,
                    password:'@word(8,16)',
                }
            }
        }
     
    }
}
export function update() {
    return {
        url: "sys/user/update",
        type: "post",
        response:{
            code:200,
        }
     
    }
}

export function setPsw() {
    return {
        url: "sys/user/setPsw",
        type: "post",
        response: (opst) => {
            return{
                code:200,
                data:{
                    name:opst.data.name,
                    password:'@word(8,16)',
                }
            }
        }
     
    }
}
export function remove() {
    return {
        url: "sys/user/delete",
        type: "post",
        response:{
            code:200,
        }
     
    }
}