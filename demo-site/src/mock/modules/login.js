import { users } from "../data"
export function login() {
    return {
        url: 'login',
        method: "post",
        response: (opts) => {
            const name = opts.data.account;
            if (
                users.find((v) => v.name === name && v.password === opts.data.password)) {
                return {
                    code: 200,
                    msg: "",
                    data: {
                        token: name + "@eyJhbGciOiJIUzUxMiJ9.eyJzdEIiOiJhZG1JhG1pbiIsImV4cC",
                        name
                    }
                };
            }
            return {
                code: -1,
                msg: "用户名或密码错误",

            };


        },
    };
}

export function logout() {
    return {
        url: "logout",
        method: "get",
        response: {
            code: 200,
            msg: null,
            data: {}
        }

    }
}