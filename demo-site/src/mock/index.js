import Mock from 'mockjs';
import config from '../request/config';
// import * as login from './modules/login'
// import * as personal from './modules/personal'
// const moduleFiles = require.context('./modules', true, /.js$/)
// import.meta.glob( '@/pages/**/*.vue', { eager: true })
// onst files = require.context('@/views', true, /\.vue$/)
const moduleFiles = import.meta.globEager("./modules/*.js") 
const modules = {};
Object.keys(moduleFiles).forEach(fileName => {
    let name = fileName.replace('.js', '')
    name = name.substring(0, name.length - 3).replace(/-(\w)/g, (L) => L.toUpperCase()).replace(/-/g, '');
    modules[name] = moduleFiles[fileName];
})
const { baseURL } = config;
// Mock.setup({ timeout })

const openMock = true
mockAll(modules, openMock)

function mockAll(modules, isOpen = true) {
    for (const k in modules) {
        mock(modules[k], isOpen)
    }

}
function mock(mod, isOpen = true) {
    if (isOpen) {
        for (var key in mod) {
            ((res) => {
                if (res.isOpen !== false) {
                    let url = baseURL
                    if (!url.endsWith("/")) {
                        url = url + "/"
                    }
                    url = url + res.url
                    Mock.mock(new RegExp(url), res.method, (opts) => {
                        opts.data = opts.body ? JSON.parse(opts.body) : null
                        const resData = Mock.mock(typeof res.response === 'function' ? res.response(opts) : res.response);
                        console.log('%cmock拦截,请求:', 'color:blue', opts)
                        console.log('%cmock拦截,响应:', 'color:blue', resData)
                        return resData;
                    })

                }
            })(mod[key]() || {})
        }
    }
}